from django.apps import AppConfig


class SabebappConfig(AppConfig):
    name = 'SabebApp'
