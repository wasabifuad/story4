from django.urls import path
from . import views

urlpatterns = [
    path ('', views.landingPage, name = 'LandingPage'),
    path ('MyProfile/', views.MyProfile, name = 'MyProfile'),
    path ('TuturTegur/', views.TuturTegur, name = 'TuturTegur'),
]