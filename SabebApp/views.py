from django.shortcuts import render

# Create your views here.
def landingPage (request):
    return render (request, 'landing-page.html')
def MyProfile (request):
    return render (request, 'profile-page.html')
def TuturTegur (request):
    return render (request, 'tutur-tegur.html')